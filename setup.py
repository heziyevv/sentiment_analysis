from pipfile import Pipfile
from setuptools import setup, find_packages, Extension
from setuptools.command.build_py import build_py
from typing import Tuple
import numpy as np
import fnmatch
from Cython.Build import cythonize

from sentiment_analysis.about import (
    __title__,
    __version__,
    __summary__,
    __author__,
    __email__,
)


__NAME__ = __title__
__VERSION__ = __version__
__LICENSE__ = "MIT"
__PYTHON_REQUIRES__ = "==3.9.*"


# --------------------------------------------------------------------------- REQUIREMENTS --------
__REQUIREMENTS__ = []

_pipfile = Pipfile.load(filename="Pipfile")
for name, specifier in _pipfile.data["default"].items():
    requirement = name
    if isinstance(specifier, str):
        if specifier[0] in {"=", "<", ">"}:
            requirement += specifier
    elif isinstance(specifier, dict):
        if "extras" in specifier and isinstance(specifier["extras"], list):
            requirement += "[" + ",".join(specifier["extras"]) + "]"
        if (
            "version" in specifier
            and isinstance(specifier["version"], str)
            and specifier["version"][0] in {"=", "<", ">"}
        ):
            requirement += specifier["version"]
    __REQUIREMENTS__.append(requirement)
__REQUIREMENTS__ = sorted(__REQUIREMENTS__)


# ---------------------------------------------------------------------------------- BUILD --------
__CMDCLASS__ = {}
__MODULE_EXCLUDE__ = []
__PACKAGE_DATA__ = {"sentiment_analysis._resources.data": ["sepen.pdf", "test.pdf"]}


# Extensions are converted to C and later to .so binaries, using Cython
__CYTHON_EXCLUDE__ = ["**/__init__.py"]
__CYTHON_LANGUAGE_LEVEL__ = 3
__CYTHON_BUILD_DIR__ = "cython/"
__CYTHON_EXTENSIONS__ = [
    Extension("sentiment_analysis.*", ["sentiment_analysis/*.py"]),
    Extension(
        "sentiment_analysis._resources.*", ["sentiment_analysis/_resources/*.py"]
    ),
    Extension(
        "sentiment_analysis._resources.data.*",
        ["sentiment_analysis/_resources/data/*.py"],
    ),
]

__EXTMODULES__ = cythonize(
    __CYTHON_EXTENSIONS__,
    exclude=__CYTHON_EXCLUDE__,
    build_dir=__CYTHON_BUILD_DIR__,
    language_level=__CYTHON_LANGUAGE_LEVEL__,
    compiler_directives={"always_allow_keywords": True},
)


def include(tup: Tuple[str, ...]) -> bool:
    """A filter that defines which .py files will be included in the distribution"""
    *_, filepath = tup
    return (
        any(
            fnmatch.fnmatchcase(filepath, pat=pattern) for pattern in __CYTHON_EXCLUDE__
        )
        or not any(
            fnmatch.fnmatchcase(filepath, pat=pattern)
            for ext in __CYTHON_EXTENSIONS__
            for pattern in ext.sources
        )
    ) and not (
        any(
            fnmatch.fnmatchcase(filepath, pat=pattern) for pattern in __MODULE_EXCLUDE__
        )
    )


if __name__ == "__main__":
    setup(
        name=__NAME__,
        description=__summary__,
        author=__author__,
        author_email=__email__,
        version=__VERSION__,
        license=__LICENSE__,
        packages=find_packages(),
        package_data=__PACKAGE_DATA__,
        install_requires=__REQUIREMENTS__,
        python_requires=__PYTHON_REQUIRES__,
        cmdclass=__CMDCLASS__,
        ext_modules=__EXTMODULES__,
    )
