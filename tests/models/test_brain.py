import unittest

import torch.nn as nn
from transformers import AutoModel, AutoTokenizer, AdamW

from sentiment_analysis.models import BERTModel, train_bert_model
from sentiment_analysis.data import create_data_loader, create_input


class TestBertModel(unittest.TestCase):
    def test_training_process(self):
        X_train, X_val, X_test, y_train, y_val, y_test = create_input(
            "tests/_resources/sample_data.csv"
        )
        tokenizer = AutoTokenizer.from_pretrained("bert-base-uncased")

        train_data_loader = create_data_loader(
            X_train.to_list(),
            y_train.to_list(),
            tokenizer,
            32,
            32,
            shuffle=True,
            num_workers=1,
        )

        base_model = AutoModel.from_pretrained("bert-base-uncased")
        model = BERTModel(
            base_model=base_model,
            output_dim=1,
            dropout=0.3,
        )

        model = model.to("cpu")
        optimizer = AdamW(model.parameters(), lr=0.001, correct_bias=False)
        loss_fn = nn.BCEWithLogitsLoss().to("cpu")

        self.assertIsNone(
            train_bert_model(1, model, train_data_loader, "cpu", loss_fn, optimizer)
        )
