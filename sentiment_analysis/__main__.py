import click
import os

import torch
import torch.nn as nn
from transformers import AutoTokenizer, AutoModel, AdamW

from .models import BERTModel, train_bert_model, eval_bert_model
from .config import Configuration
from .util import chain_decorators
from .data import create_input, create_data_loader


MODEL_PATH = 'ckpt/model.pt'
os.environ["TOKENIZERS_PARALLELISM"] = "false"


@click.command()
@chain_decorators(Configuration.add_options_to_click())
def main(**kwargs):
    X_train, X_val, X_test, y_train, y_val, y_test = create_input(kwargs["train_csv"])
    X_train, X_val, y_train, y_val = (
        X_train[:500],
        X_val[:100],
        y_train[:500],
        y_val[:100],
    )
    device = "cpu"
    tokenizer = AutoTokenizer.from_pretrained("bert-base-uncased")

    train_data_loader = create_data_loader(
        X_train.to_list(),
        y_train.to_list(),
        tokenizer,
        kwargs["max_input_length"],
        kwargs["batch_size"],
        shuffle=True,
        num_workers=1,
    )

    val_data_loader = create_data_loader(
        X_val.to_list(),
        y_val.to_list(),
        tokenizer,
        kwargs["max_input_length"],
        kwargs["batch_size"],
        shuffle=True,
        num_workers=1,
    )

    base_model = AutoModel.from_pretrained("bert-base-uncased")
    model = BERTModel(
        base_model=base_model,
        output_dim=kwargs["output_dim"],
        dropout=kwargs["dropout"],
    )

    model = model.to(device)
    optimizer = AdamW(
        model.parameters(), lr=kwargs["learning_rate"], correct_bias=False
    )
    loss_fn = nn.BCEWithLogitsLoss().to(device)

    for epoch in range(kwargs["num_epochs"]):
        train_bert_model(epoch, model, train_data_loader, device, loss_fn, optimizer)
        eval_bert_model(epoch, model, val_data_loader, device, loss_fn)

    torch.save(model, MODEL_PATH)


if __name__ == "__main__":
    main()
