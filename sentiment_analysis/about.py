#!/usr/bin/env python
# -*- coding: utf-8 -*-


__MAJOR__ = 1
__MINOR__ = 0
__PATCH__ = 3
__PRERELEASE__ = ""


__title__ = "sentiment-analysis-firuz"
__version__ = (
    ".".join([str(__MAJOR__), str(__MINOR__), str(__PATCH__)]) + __PRERELEASE__
)
__summary__ = "Relatively Simple ML project from start to end"
__author__ = "Farid Haziyev"
__email__ = "ferid.heziyev@hotmail.com"


if __name__ == "__main__":
    print(__version__)
