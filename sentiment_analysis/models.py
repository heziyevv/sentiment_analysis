import torch.nn as nn
import torch

from .util import calculate_accuracy
import spacy

nlp = spacy.load("en_core_web_sm")


class BERTModel(nn.Module):
    def __init__(self, base_model, output_dim, dropout=0.3):
        super().__init__()
        self.base_model = base_model
        embedding_dim = self.base_model.config.to_dict()["hidden_size"]
        self.fc = nn.Linear(embedding_dim, output_dim)
        self.dropout = nn.Dropout(dropout)

    def forward(self, input_ids, attention_mask):
        pooled_out = self.base_model(input_ids, attention_mask)
        out = self.dropout(pooled_out[1])
        out = self.fc(out)
        return out.squeeze(1)


def train_bert_model(epoch, model, data_loader, device, loss_fn, optimizer):
    total_train_loss = 0.0
    total_train_accuracy = []
    model = model.train()

    for data in data_loader:
        xs = data["input_ids"].to(device)
        ys = data["labels"].to(device)
        attention_mask = data["attention_mask"].to(device)

        predicted = model(xs, attention_mask)
        train_loss = loss_fn(predicted, ys)

        optimizer.zero_grad()
        train_loss.backward()
        optimizer.step()
        total_train_loss += train_loss.item()
        accuracy = calculate_accuracy(predicted.cpu(), ys.cpu())
        total_train_accuracy.append(accuracy)

    print(f"Epoch {epoch}, Training Loss:  {total_train_loss}")
    print(
        f"Epoch {epoch}, Training accuracy: {sum(total_train_accuracy) / len(total_train_accuracy)}"
    )


def eval_bert_model(epoch, model, data_loader, device, loss_fn):
    total_val_loss = 0.0
    total_val_accuracy = []
    model = model.eval()

    with torch.no_grad():
        for data in data_loader:
            xs = data["input_ids"].to(device)
            ys = data["labels"].to(device)
            attention_mask = data["attention_mask"].to(device)
            predicted = model(xs, attention_mask)
            val_loss = loss_fn(predicted, ys)

            total_val_loss += val_loss.item()
            accuracy = calculate_accuracy(predicted.cpu(), ys.cpu())
            total_val_accuracy.append(accuracy)
    print(f"Epoch {epoch}, Validation loss: {total_val_loss}")
    print(
        f"Epoch {epoch}, Validation accuracy: {sum(total_val_accuracy) / len(total_val_accuracy)}"
    )
