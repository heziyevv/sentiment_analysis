import os
import click
from typing import Any, Dict


hyper_parameters = {
    "learning_rate": 0.001,
    "batch_size": 32,
    "num_epochs": 5,
    "dropout": 0.3,
    "output_dim": 1,
    "max_input_length": 32,
    "train_csv": "raw_data/imdb_dataset.csv",
    "device": "cpu",
}


class Configuration:
    arguments: Dict[str, Any] = {}
    options: Dict[str, Any] = {}

    for parameter, default in hyper_parameters.items():
        options[parameter.replace("_", "-")] = default

    @classmethod
    def add_options_to_click(cls):
        return [
            click.option(f"--{option}", default=default)
            for option, default in cls.options.items()
        ]
