import re
import string
import random

from typing import Callable
import numpy as np
import torch


def chain_decorators(decorators: list[Callable]):
    def _add_decorators(func: Callable):
        for decorator in decorators:
            func = decorator(func)
        return func

    return _add_decorators


def initialize():
    torch.manual_seed(0)
    np.random.seed(0)
    random.seed(0)


class TextProcessor:
    @staticmethod
    def remove_url(text):
        url = re.compile(r"https?://\S+|www\.\S+")
        return url.sub(r"", text)

    @staticmethod
    def remove_html(text):
        html = re.compile(r"<.*?>")
        return html.sub(r"", text)

    @staticmethod
    def remove_punct(text):
        table = str.maketrans("", "", string.punctuation)
        return text.translate(table)

    @staticmethod
    def remove_emoji(text):
        emoji_pattern = re.compile(
            "["
            u"\U0001F600-\U0001F64F"  # emoticons
            u"\U0001F300-\U0001F5FF"  # symbols & pictographs
            u"\U0001F680-\U0001F6FF"  # transport & map symbols
            u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
            u"\U00002702-\U000027B0"
            u"\U000024C2-\U0001F251"
            "]+",
            flags=re.UNICODE,
        )
        return emoji_pattern.sub(r"", text)

    @classmethod
    def cleanup_text(cls, line: str) -> str:
        """
        Args:
            line: text line of the opinion
        Returns:
            text line where not alphabetical characters removed
        """
        corrections = {"&quot": "", "&amp": ""}
        line_lower = line.lower()

        for wrong, cor in corrections.items():
            line_lower = line_lower.replace(wrong, cor)

        # remove html
        line_lower = cls.remove_html(line_lower)

        # remove url
        line_lower = cls.remove_url(line_lower)

        # remove emoji
        line_lower = cls.remove_emoji(line_lower)
        # words = re.findall(r"[\w']+", line_lower)

        return cls.remove_punct(line_lower)


def calculate_accuracy(y_pred, y):
    y = y.detach().numpy()
    y_pred = torch.sigmoid(y_pred).detach().numpy()
    y_pred[y_pred > 0.5] = 1
    y_pred[y_pred < 0.5] = 0
    correct = len(y_pred[y_pred == y])
    return (correct / y_pred.shape[0]) * 100
