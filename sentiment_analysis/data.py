import pandas as pd
import torch
from torch.utils.data import Dataset, DataLoader
from sklearn.model_selection import train_test_split

from .util import initialize, TextProcessor


class SentimentDataset(Dataset):
    def __init__(self, tweets, labels, tokenizer, max_len):
        self.tweets = tweets
        self.labels = labels
        self.tokenizer = tokenizer
        self.max_len = max_len

    def __len__(self):
        return len(self.tweets)

    def __getitem__(self, idx):
        tweet = self.tweets[idx]
        label = self.labels[idx]

        encoding = self.tokenizer.encode_plus(
            tweet,
            add_special_tokens=True,
            max_length=self.max_len,
            return_token_type_ids=False,
            padding="max_length",
            return_attention_mask=True,
            truncation=True,
            return_tensors="pt",
        )

        return {
            "input_ids": encoding["input_ids"].flatten(),
            "attention_mask": encoding["attention_mask"].flatten(),
            "labels": torch.tensor(label, dtype=torch.float32),
        }


def create_input(filename):
    initialize()
    df = pd.read_csv(filename)

    df["review"] = df["review"].apply(TextProcessor.cleanup_text)
    df["sentiment"] = df["sentiment"].apply(lambda x: 1 if x == "positive" else 0)

    X_train, X_val, y_train, y_val = train_test_split(
        df["review"], df["sentiment"], test_size=0.1
    )
    X_val, X_test, y_val, y_test = train_test_split(X_val, y_val, test_size=0.05)
    return X_train, X_val, X_test, y_train, y_val, y_test


def create_data_loader(
    x_data,
    label_data,
    tokenizer,
    max_length,
    batch_size,
    shuffle,
    num_workers,
):
    dataset = SentimentDataset(x_data, label_data, tokenizer, max_length)
    return DataLoader(
        dataset, batch_size=batch_size, shuffle=shuffle, num_workers=num_workers
    )
