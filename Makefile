.PHONY: unittest
unittest:
	python -m pytest tests

.PHONY: download-resources
download-resources:
	spacy download en_core_web_sm

.PHONY: build
build:
	python setup.py sdist
	twine upload --repository testpypi --verbose dist/*.tar.gz

